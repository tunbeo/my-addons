# -*- coding: utf-8 -*-
{
    'name': "Dark Backend Theme",
    'summary': """Dark Backend Theme""",

    'description': """Dark Backend Theme""",

    'author': "Odoo Dev Free",
    'category': 'Theme/Backend',
    'version': '14.0.0.0.1',
    'license': 'OPL-1',
    'price': 0,
    'currency': 'USD',
    'support': 'odoo.dev.free@gmail.com',
    'images': ['static/description/odf-dark-backend-theme-banner.png'],
    'depends': ['base', 'mail', 'web'],
    'data': [
        'views/dark_theme.xml',
    ],
}
